package pdx.com.nikeelevatorpitch;

import java.util.List;

/**
 * Created by Jacob on 7/26/17.
 */

public interface CategoriesCallback {
    public void onResult(List<Category> categories);
}
