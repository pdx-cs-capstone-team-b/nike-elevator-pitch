package pdx.com.nikeelevatorpitch;

/**
 * Enum for indicating product level type
 * @author Eli Cook
 */

public enum ProductFragmentType {
    PRODUCT_LVL_1, PRODUCT_LVL_2, PRODUCT_LVL_3;
}
